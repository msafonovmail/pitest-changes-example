plugins {
    id("java")
    id("application")
    id("info.solidsoft.pitest") version "1.9.11"

}

group = "com.gitlab.mikesafonov"

repositories {
    mavenCentral()
}

dependencies {
    pitest("com.github.mikesafonov:pitest-git-changes-plugin:0.0.3")
    pitest("com.github.mikesafonov:pitest-git-changes-report-gitlab-plugin:0.0.3")

    val junitVersion = "5.8.1"

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
    testImplementation("ch.qos.logback:logback-classic:1.3.3")
}

tasks.test {
    useJUnitPlatform()
}

pitest {
    pitestVersion.set("1.11.7")
    features.set(listOf("+git-changes(target[HEAD^])"))
    targetClasses.set(listOf("com.gitlab.mikesafonov.second.*"))
    targetTests.set(listOf("com.gitlab.mikesafonov.second.*"))
    outputFormats.set(listOf("GITLAB"))
    junit5PluginVersion.set("1.1.2")
    timestampedReports.set(false)
    if(System.getenv("CI").toBoolean()) {
        pluginConfiguration.set(
                mapOf(
                        "PROJECT_NAME" to "test-project-second",
                        "GITLAB_TOKEN" to System.getenv("GITLAB_TOKEN"),
                        "GITLAB_PROJECT_ID" to System.getenv("CI_PROJECT_ID"),
                        "GITLAB_URL" to System.getenv("CI_SERVER_URL"),
                        "GITLAB_MR_ID" to System.getenv("CI_OPEN_MERGE_REQUESTS")
                )
        )
    }
}
